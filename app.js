let globalVariables = {};

module.exports =  (name, obj) => {
    if(!obj) return globalVariables[name];
    else if(!obj && !name) return this;
    else{
        globalVariables[name] = obj;
    }
}
